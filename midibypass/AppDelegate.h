//
//  AppDelegate.h
//  midibypass
//
//  Created by martin on 11/4/14.
//  Copyright (c) 2014 foundation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

